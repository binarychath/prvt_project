package itsm_pharmacy_test_files.shipping_tab;

import itsm_pharmacy_base_files.app_manager.ApplicationManager;
import itsm_pharmacy_base_files.app_manager.model_data.ShippingTabData;

import static itsm_pharmacy_base_files.app_manager.ApplicationManager.*;
import static itsm_pharmacy_base_files.framework.global_parameters.GlobalParameters.*;
import static itsm_pharmacy_test_files.entry_tab.EntryTabSingleRxWorkflow.RxId;

public class ShippingTabWorkflow {

    private ShippingTabData shippingTabData = new ShippingTabData();

    public void enter_RxId_And_click_Save_Weight_Btn(ApplicationManager app) throws InterruptedException {
        log.info("");
        Thread.sleep(LONG_WAIT);
        app.getNavigationServicePharmMain().goToShippingTab();
        app.getShippingTabService().enterRxId(shippingTabData.rxid(RxId));
        app.getShippingTabService().clickSubmitBtn();
        app.getShippingTabService().clickSaveWeightBtn();
        app.getShippingTabService().getSuccessMessage();
    }

    public void check_OrderHist_And_Click_Save_OrderBtn(ApplicationManager app) throws InterruptedException {
        log.info("");
        app.getShippingTabService().clickOrderHistoryBtn();
        reportLog("Check whether Order Status is 'Approved' and Action is 'Approve' or not");
        app.getShippingTabService().getStatusOfTheOrder();
        Thread.sleep(LONG_WAIT);
        app.getShippingTabService().clickSaveOrderBtn();
    }

    public void select_Shipping_Method_And_Click_Finalize_Btn(ApplicationManager app) throws InterruptedException {
        log.info("");
        app.getShippingTabService().selectShippingMethod(1);//Select shipping method 'Home Delivery'
        app.getShippingTabService().clickSaveShippingMethodBtn();
        Thread.sleep(LONG_WAIT*2);
        app.getShippingTabService().clickFinalizedBtn();
        app.getShippingTabService().getSuccessMessage();
        Thread.sleep(LONG_WAIT);
    }
}
