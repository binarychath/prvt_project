package itsm_pharmacy_test_files.order_tab;

import itsm_pharmacy_base_files.app_manager.model_data.OrderTabData;
import itsm_pharmacy_base_files.app_manager.test_base.TestBase;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

import java.text.ParseException;

import static itsm_pharmacy_base_files.app_manager.ApplicationManager.*;
import static itsm_pharmacy_base_files.framework.global_parameters.GlobalParameters.*;

public class OrderTabValidationTest extends TestBase {

    private int orderId;

    @Priority(1)
    @Test(priority = 1)
    public void select_New_Order_From_Queues_Tab_Test() throws InterruptedException {
        reportLog("****** Select New Order From Queues Tab *****");
        app.getNavigationServicePharmMain().goToPharmacyMainPage();
        app.getNavigationServicePharmMain().goToQueuesTab();
        app.getNavigationServiceQueuesTabs().goToTotalPendingOrders();
        Thread.sleep(LONG_WAIT);
        app.getNavigationServiceQueuesTabs().selectNewOrderFromQueues();
        orderId = app.getOrderTabService().getOrderId();
    }

    @Priority(2)
    @Test(priority = 2)
    public void check_Availability_Of_EntryTab_Image_Test(){
        log.info("");
        reportLog("**** Check Availability of Order Tab Image *****");
        app.getOrderTabService().checkOrderTabImage();
    }

    @Priority(3)
    @Test(priority = 3)
    public void check_Panel_Heading_Buttons_Test() throws InterruptedException {
        log.info("");
        reportLog("****** Check Availability of Order tab icon and Buttons in panel-heading *****");
        app.getOrderTabService().checkPanelHeadingButtons();
    }

    @Priority(4)
    @Test(priority = 4)
    public void check_Order_Date_Test() throws InterruptedException, ParseException {
        log.info("");
        reportLog("****** Check Order Date is in correct format or not *****");
//        app.getOrderTabService().checkDuplicateDate();
        app.getOrderTabService().checkOrderDate();
        app.getSelectorService().assertSuccessMessage("Date updated!");
        driver.navigate().refresh();
        app.getNavigationServicePharmMain().goToPharmacyMainPage();
        app.getNavigationServicePharmMain().goToOrderTab();
    }

    @Priority(5)
    @Test(priority = 5)
    public void check_Shipping_Methods_Test() throws InterruptedException {
        log.info("");
        reportLog("****** Check availability of Shipping Methods *****");
        app.getOrderTabService().checkShippingMethods();
        app.getOrderTabService().clickSaveShippingMethodsButton();
        Thread.sleep(LONG_WAIT);
        app.getSelectorService().assertSuccessMessage("Shipping method saved");
    }

    @Priority(6)
    @Test(priority = 6)
    public void check_NDC_Field_And_Select_NDC_Test() {
        log.info("");
        reportLog("****** Check NDC field and select NDC from Select NDC widget *****");
        app.getOrderTabService().selectNDC();
        app.getOrderTabService().clickSaveShippingMethodsButton();
    }

    @Priority(7)
    @Test(priority = 7)
    public void check_RxCondition_Buttons_Test() throws InterruptedException {
        log.info("");
        reportLog("****** Check Rx Condition Button & it's availability *****");
        app.getOrderTabService().clickRxConditionButton();
        app.getOrderTabService().checkRxConditionButton();
        reportLog("");
        app.getOrderTabService().clickCallPatientButton();
        app.getOrderTabService().checkCallPatientButton();
        reportLog("");
        app.getOrderTabService().clickBillingConfigureOrderButton();
        app.getOrderTabService().checkBillingConfigureOrderButton();
        app.getSelectorService().assertSuccessMessage("Bill configuration are saved!");
    }

    @Priority(8)
    @Test(priority = 8)
    public void submit_Order_Test() throws InterruptedException {
        log.info("");
        reportLog("****** Click submit button if there are no 'Warning!' Messages ******");
        if(driver.findElements(By.xpath("//*[contains(text(),'Warning!')]")).size()>0
                && !driver.findElement(By.id("rx_order_submit")).isEnabled()) {
            reportLog("Contains 'Warning!' Notifications");
        }else {
            app.getOrderTabService().clickSubmitButton(new OrderTabData().OrderId(orderId));
        }
    }


}
